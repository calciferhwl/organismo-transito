package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author salasistemas
 */
public class Agente extends Persona{
    // atributos
    private short placa;

    public Agente(long identificacion, String nombre, String apellido , short placa) throws Exception {
        super(identificacion, nombre, apellido);
        if (placa <= 0 || placa > 9999){
            throw new Exception ("EL numero de placa debe ser mayor a cero y debe tener 4 digitos\n");
        }
        this.placa = placa;
    }
    
    // GETERS
    
    public short getPlaca() {
        return placa;
    }
    
    // SETERS
    
    public void setPlaca(short placa) throws Exception{
        if (placa <= 0 || placa > 9999){
            throw new Exception ("EL numero de placa debe ser mayor a cero \n");
        }
        this.placa = placa;
    }

    
    
    // EQUAL
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agente other = (Agente) obj;
        if (this.placa != other.placa) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.placa;
        return hash;
    }
    
    
}
