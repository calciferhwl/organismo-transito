package modelo;


import java.time.LocalDate;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sebastian
 */
public class Vehiculo {
    // Atributos
    private String placa;
    private short modelo;
    private String color;
    private String marca;
    
    // métodos

    public Vehiculo(String placa, short modelo, String color, String marca) throws Exception{
        
        // VERIFICACION
        if(color == null || "".equals(color.trim())){
            throw new Exception ("El modelo no debe ser nulo o vacio \n");
        }
        if(placa == null || "".equals(placa.trim())){
            throw new Exception ("La placa no debe ser nula o vacia \n");
        }
        if(marca == null || "".equals(marca.trim())){
            throw new Exception ("La marca no debe ser nula o vacia \n");
        }
        // 1900 < modelo < añoActual + 1
        int añoValido = LocalDate.now().getYear() + 1;
        if(1900 >= modelo || modelo >= añoValido) {
            throw new Exception ("El modelo del auto debe ser mayor a 1900 y menor al año " + 
                    añoValido);
        }
        
        if(placa.length() != 6){
            throw new Exception ("La placa debe tener 6 digitos \n");
        }
        
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
    }
    
    // geters...
    public String getPlaca() {
        return placa;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }
    
    // setters...

    public void setPlaca(String placa) throws Exception{
        
        if (placa == null || "".equals(placa.trim())){
            throw new Exception ("la placa no puede ser nulla o vacia \n");
        }
        if(placa.length() != 6){
            throw new Exception ("La placa debe tener 6 digitos \n");
        }
        this.placa = placa;
    }

    public void setModelo(short modelo) throws Exception{
        
        int añoValido = LocalDate.now().getYear() + 1;
        if(1900 >= modelo || modelo >= añoValido) {
            throw new Exception ("El modelo del auto debe ser mayor a 1900 y menor o igual  al año " + 
                    añoValido);
        }
        this.modelo = modelo;
    }

    public void setColor(String color) throws Exception{
        
        if(color == null || "".equals(color.trim())){
            throw new Exception ("El modelo no debe ser nulo o vacio \n");
        }
        this.color = color;
    }

    public void setMarca(String marca) throws Exception{
        
        if(marca == null || "".equals(marca.trim())){
            throw new Exception ("La marca no debe ser nula o vacia \n");
        }
        this.marca = marca;
    }


    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        return true;
    }
    
    
}
