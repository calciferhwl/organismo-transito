package modelo;


import java.util.LinkedList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author salasistemas
 */
public class OrganismoTransito {

    private List<MotivoMulta> motivosMultas = new LinkedList<>();
    private List<Agente> agentes = new LinkedList<>();
    private List<Licencia> licencias = new LinkedList<>();
    private List<Vehiculo> vehiculos = new LinkedList<>();
    private List<Multa> multas = new LinkedList<>();

    //Métodos 
    public OrganismoTransito() {

    }

   

    //////////////////////////////////////////////////////////////////////////////////////////////
    // ACCIONES CON MULTAS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    public Multa BuscarMulta(Multa multa) throws Exception {

        if (multas.contains(multa)) {
            return multas.get(multas.indexOf(multa));
        } else {
            throw new Exception("La multa no se encuentra registrada. \n");
        }

    }

    public void AddMulta(Multa multa) throws Exception {
        
        if (multas.contains(multa)) {
            throw new Exception("La multa ya se encuentra registrada \n");
        }
        multas.add(multa);
    }

    public void QuitarMulta(Multa multa) throws Exception {
        

        if (multas.contains(multa)) {
            multas.remove(multa);
        } else {
            throw new Exception("La multa no se encuentra registrada \n");
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // ACCIONES CON MOTIVOS MULTAS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    public void AddMotivoMulta(MotivoMulta motivo) throws Exception {

        if (motivosMultas.contains(motivo)) {
            throw new Exception("El motivo de la multa ya se encuentra registrado en esta. \n");
        }

        motivosMultas.add(motivo);
        System.out.println(" Registro satisfactorio. \n");
    }

    public MotivoMulta BuscarMotivoMUlta(MotivoMulta motivo) throws Exception {

        if (motivosMultas.contains(motivo)) {
            return motivosMultas.get(motivosMultas.indexOf(motivo));
        } else {
            throw new Exception("El motivo de la  multa no se encuentra registrado. \n");
        }

    }

    public void QuitarMotivoMulta(MotivoMulta motivo) throws Exception {

        if (motivosMultas.contains(motivo)) {
            motivosMultas.remove(motivo);
        } else {
            throw new Exception("El motivo no se encuentra registrado \n");
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    // ACCIONES CON AGENTES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    public void AddAgente(Agente agente) throws Exception {

        if (agentes.contains(agente)) {
            throw new Exception("El agente que intentas registrar, ya se encuentra registrado. \n");
        }
        agentes.add(agente);
        System.out.println(" Registro satisfactorio. \n");
    }

    public Agente BuscarAgente(Agente agente) throws Exception {

        if (agentes.contains(agente)) {
            return agentes.get(agentes.indexOf(agente));
        } else {
            throw new Exception("El agente no se encuentra registrado. \n");
        }
    }
    
    public Agente BuscarAgentePlaca (Short Placa) throws Exception{
        int iterador = 0;
        if (agentes == null){
            throw new Exception ("Actualmente la lista de agentes está vacia \n");
        }else{
            while(iterador <= agentes.size()){
                if(agentes.get(iterador).getPlaca() == Placa){
                    return agentes.get(iterador);
                }else{
                    iterador++;
                }
            }
            throw new Exception("No se ha encontrado el agente \n");
        }
    }
    
    public void QuitarAgente(Agente agente) throws Exception {

        if (agentes.contains(agente)) {
            agentes.remove(agente);
        } else {
            throw new Exception("El agente no se encuentra registrado \n");
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // ACCIONES CON LICENCIAS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    public void AddLicencia(Licencia licencia) throws Exception {

        if (licencias.contains(licencia)) {
            throw new Exception("La licencia ya se encuentra registrada. \n");
        }
        licencias.add(licencia);
        System.out.println(" Registro satisfactorio. \n");
    }

    public Licencia BuscarLicencia(Licencia licencia) throws Exception {

        if (licencias.contains(licencia)) {
            return licencias.get(licencias.indexOf(licencia));
        } else {
            throw new Exception("La licencia no se encuentra registrada. \n");
        }
    }
    
    public List<Licencia> BuscarLicencias(Long identificacion){
        List<Licencia> licenciasEncontradas = new LinkedList<>();
        
        for(Licencia licencia : this.licencias){
            if (licencia.getPersona().getIdentificacion() == identificacion){
                licenciasEncontradas.add(licencia);
            }
        }
        return licenciasEncontradas;
    }
    public void QuitarLicencia(Licencia licencia) throws Exception {

        if (licencias.contains(licencia)) {
            licencias.remove(licencia);
        } else {
            throw new Exception("La licencia  no se encuentra registrada \n");
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // ACCIONES CON VEHICULOS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    public void AddVehiculo(Vehiculo vehiculo) throws Exception {

        if (vehiculos.contains(vehiculo)) {
            throw new Exception("El vehiculo ya se encuentra registrado \n");
        }
        vehiculos.add(vehiculo);
        System.out.println(" Registro satisfactorio. \n");
        System.out.println(vehiculos.get(0).getPlaca());
    }

    public Vehiculo BuscarVehiculo(Vehiculo vehiculo) throws Exception {

        if (vehiculos.contains(vehiculo)) {
            return vehiculos.get(vehiculos.indexOf(vehiculo));
        } else {
            throw new Exception("El Vehiculo no se encuentra registrado. \n");
        }
    }
    public Vehiculo BuscarVehiculoPlaca(String placa) throws Exception {
        int iterador = 0;
        if(vehiculos == null){
            throw new Exception ("La lista de vehiculos actualmente está vacia \n");
        }else{
        while(iterador <= vehiculos.size()){
            if(vehiculos.get(iterador).getPlaca().equals(placa)){
                return vehiculos.get(iterador);
            }
            iterador += 1;
            }
        }
        throw new Exception ("No se encontró el vehiculo \n");
    }
    
    public void QuitarVehiculo(Vehiculo vehiculo) throws Exception {

        if (vehiculos.contains(vehiculo)) {
            vehiculos.remove(vehiculo);
        } else {
            throw new Exception("El vehiculo  no se encuentra registrada \n");
        }
    }

}
