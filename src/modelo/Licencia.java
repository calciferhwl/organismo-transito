package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Date;
import java.util.Objects;
/**
 *
 * @author salasistemas
 */
public class Licencia {
    // Atributos
    private Categoria categoria;
    private Persona persona;
    private Date date;

    public Licencia(Categoria categoria, Persona persona, Date date) {
        
        this.categoria = categoria;
        this.persona = persona;
        this.date = date;
    }

    public Licencia() {
        
    }
    
    // GETERS
    
    public Categoria getCategoria() {
        return categoria;
    }

    public Persona getPersona() {
        return persona;
    }

    public Date getDate() {
        return date;
    }
    
    // SETERS
    
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    // EQUAL
    
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Licencia other = (Licencia) obj;
        if (this.categoria != other.categoria) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
    
    
}
