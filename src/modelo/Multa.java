package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
/**
 *
 * @author salasistemas
 */
public class Multa {
    // Atributos
    private Date date;
    private Vehiculo vehiculo;
    private Persona conductor;
    private Agente agente;
    private int valor;
    private List<MotivoMulta> motivosMultas = new LinkedList<>();
    
    
    // Métodos
    public Multa(Date date, Vehiculo vehiculo, Persona conductor, Agente agente , int valor) throws Exception {
        
        if (valor < 0){
            throw new Exception ("El valor debe ser mayor a cero \n");
        }
        this.date = date;
        this.vehiculo = vehiculo;
        this.conductor = conductor;
        this.agente = agente;
        this.valor = valor;
    }
    
    public void AddMotivoMulta(MotivoMulta motivo) throws Exception{
 
            if (motivosMultas.contains(motivo)){
                throw new Exception ("El motivo de la multa ya se encuentra registrado en esta. \n");
            }

        motivosMultas.add(motivo);
    }

    public Date getDate() {
        return date;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Persona getConductor() {
        return conductor;
    }

    public Agente getAgente() {
        return agente;
    }

    public int getValor() {
        return valor;
    }

    
    public List<MotivoMulta> getMotivosMultas() {
        return motivosMultas;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Multa other = (Multa) obj;
        if (this.valor != other.valor) {
            return false;
        }
        
        if (!Objects.equals(this.vehiculo, other.vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.conductor, other.conductor)) {
            return false;
        }
        if (!Objects.equals(this.agente, other.agente)) {
            return false;
        }
        if (!Objects.equals(this.motivosMultas, other.motivosMultas)) {
            return false;
        }
        return true;
    }
    
    
}
