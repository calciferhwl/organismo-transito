package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sebastian
 */
public class Persona {

    // Atributos
    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido) throws Exception{
        
        if(identificacion <= 1000000000L || identificacion >= 99999999999L){
            throw new Exception ("EL numero de identificacion debe tener 10 o 11 digitos \n");
        }
        if(nombre == null || "".equals(nombre.trim())){
            throw new Exception ("El nombre no debe ser nulo o vacio \n");
        }
        if(apellido == null || "".equals(apellido.trim())){
            throw new Exception ("El apellido no debe ser nulo o vacio \n");
        }
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    // GETERS
    
    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    // SETERS
    
    public void setIdentificacion(long identificacion) throws Exception{
        
        if(identificacion <= 1000000000L || identificacion >= 99999999999L){
            throw new Exception ("EL numero de identificacion debe tener 10 o 11 digitos \n");
        }
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) throws Exception {
        
        if(nombre == null || "".equals(nombre.trim())){
            throw new Exception ("El nombre no debe ser nulo o vacio \n");
        }
        this.nombre = nombre;
    }

    public void setApellido(String apellido) throws Exception {
        
        
        if(apellido == null || "".equals(apellido.trim())){
            throw new Exception ("El apellido no debe ser nulo o vacio \n");
        }
        this.apellido = apellido;
    }

    // EQUAL

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.identificacion != other.identificacion) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + (int) (this.identificacion ^ (this.identificacion >>> 32));
        return hash;
    }
    
}
