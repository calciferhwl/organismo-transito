/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import modelo.OrganismoTransito;
import modelo.Vehiculo;

/**
 *
 * @author sebastian
 */
public class Principal extends javax.swing.JFrame {

    private OrganismoTransito transito;
    private Vehiculo vehiculo;
    
    
    /**
     * Creates new form Principal
     *
     * @param org
     */
    public Principal(OrganismoTransito org) {
        this.transito = org;
      
        initComponents();
        // manejador de eventos para el item del menú archivo
        itemSalir.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        
        
        // manejador de eventos para el item del menú archivo
        itemAcercade.addActionListener(new ActionListener() {
            String msg = "Programa creado por : " + "\n" + "Sebastian Ortiz";

            @Override

            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(rootPane, msg);
            }

        });

        // manejador de eventos para el item registrar vehiculo
        RegistrarVehiculoAL registroVehiculoAL = new RegistrarVehiculoAL();
        registrarVehiculo.addActionListener(registroVehiculoAL);
        
        // manejador de eventos para el item registrar licencia
        RegistrarLicenciaAL registroLicenciaAL = new RegistrarLicenciaAL();
        registrarLicencia.addActionListener(registroLicenciaAL);
        
        // manejador de eventos para el item registrar agente
        RegistrarAgenteAL registroAgenteAL = new RegistrarAgenteAL();
        registraragente.addActionListener(registroAgenteAL);
        // accediendo al boton cancelar de la ventana "licencia_ui"
        // VENTANADESKOPT : es la ventana que se abre al presionar en el menú "Registrar Licencia"
        
        // manejador de eventos para el item registrar multa
        RegistrarMultaAL registroMultaAL = new RegistrarMultaAL();
        registrarMulta.addActionListener(registroMultaAL);
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollBar1 = new javax.swing.JScrollBar();
        jSeparator1 = new javax.swing.JSeparator();
        escritorio2 = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        archivo = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        itemSalir = new javax.swing.JMenuItem();
        licencia = new javax.swing.JMenu();
        registrarLicencia = new javax.swing.JMenuItem();
        multa = new javax.swing.JMenu();
        registrarMulta = new javax.swing.JMenuItem();
        agente = new javax.swing.JMenu();
        registraragente = new javax.swing.JMenuItem();
        vehiculoMenu = new javax.swing.JMenu();
        registrarVehiculo = new javax.swing.JMenuItem();
        ayuda = new javax.swing.JMenu();
        itemAcercade = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Organismo de Transito");

        escritorio2.setBackground(new java.awt.Color(38, 40, 55));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Sources/esperanding.gif"))); // NOI18N

        escritorio2.setLayer(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout escritorio2Layout = new javax.swing.GroupLayout(escritorio2);
        escritorio2.setLayout(escritorio2Layout);
        escritorio2Layout.setHorizontalGroup(
            escritorio2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(escritorio2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        escritorio2Layout.setVerticalGroup(
            escritorio2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getContentPane().add(escritorio2, java.awt.BorderLayout.CENTER);

        jMenuBar1.setBackground(new java.awt.Color(38, 40, 55));
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));

        archivo.setForeground(new java.awt.Color(255, 255, 255));
        archivo.setText("Archivo");
        archivo.add(jSeparator2);

        itemSalir.setText("Salir");
        archivo.add(itemSalir);

        jMenuBar1.add(archivo);

        licencia.setForeground(new java.awt.Color(255, 255, 255));
        licencia.setText("Licencias");

        registrarLicencia.setText("Registrar Licencia");
        licencia.add(registrarLicencia);

        jMenuBar1.add(licencia);

        multa.setForeground(new java.awt.Color(255, 255, 255));
        multa.setText("Multas");
        multa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                multaItemStateChanged(evt);
            }
        });

        registrarMulta.setText("Registrar Multa");
        multa.add(registrarMulta);

        jMenuBar1.add(multa);

        agente.setForeground(new java.awt.Color(255, 255, 255));
        agente.setText("Agentes");

        registraragente.setText("Registrar Agente");
        registraragente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registraragenteActionPerformed(evt);
            }
        });
        agente.add(registraragente);

        jMenuBar1.add(agente);

        vehiculoMenu.setForeground(new java.awt.Color(255, 255, 255));
        vehiculoMenu.setText("Vehiculos");

        registrarVehiculo.setText("Registrar Vehiculo");
        vehiculoMenu.add(registrarVehiculo);

        jMenuBar1.add(vehiculoMenu);

        ayuda.setForeground(new java.awt.Color(255, 255, 255));
        ayuda.setText("Ayuda");

        itemAcercade.setText("Acerca de");
        ayuda.add(itemAcercade);

        jMenuBar1.add(ayuda);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void registraragenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registraragenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_registraragenteActionPerformed

    private void multaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_multaItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_multaItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu agente;
    private javax.swing.JMenu archivo;
    private javax.swing.JMenu ayuda;
    private javax.swing.JDesktopPane escritorio2;
    private javax.swing.JMenuItem itemAcercade;
    private javax.swing.JMenuItem itemSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JMenu licencia;
    private javax.swing.JMenu multa;
    private javax.swing.JMenuItem registrarLicencia;
    private javax.swing.JMenuItem registrarMulta;
    private javax.swing.JMenuItem registrarVehiculo;
    private javax.swing.JMenuItem registraragente;
    private javax.swing.JMenu vehiculoMenu;
    // End of variables declaration//GEN-END:variables
    
    

    // manejador de eventos para el item "registrarvehiculo" del menu
    public class RegistrarVehiculoAL implements ActionListener {

        private Vehiculo_ui registrarvehiculo = null;
        
        @Override
        public void actionPerformed(ActionEvent evt) {
            
                if(registrarvehiculo == null){
                    this.registrarvehiculo = new Vehiculo_ui(transito);
                    escritorio2.add(registrarvehiculo);
                    // poner la ventana y el desktop del tamaño de la ventana
                    Redimensionar(registrarvehiculo);
                }
                
                registrarvehiculo.setVisible(true);
                
        }
    }
    
    // manejador de eventos para el item "registrarlicencia" del menu
    public class RegistrarLicenciaAL implements ActionListener {

        private Licencia_UI registrarlicencia = null;
        @Override
        public void actionPerformed(ActionEvent evt) {
                    
            if(registrarlicencia == null){
                    registrarlicencia = new Licencia_UI(transito);
                    escritorio2.add(registrarlicencia);
                    // poner la ventana y el desktop del tamaño de la ventana
                    Redimensionar(registrarlicencia);
            }
            registrarlicencia.setVisible(true);
        }
    }
    
    // manejador de eventos para el item "registraragente" del menu
    public class RegistrarAgenteAL implements ActionListener {

        private Agente_ui registraragente = null;
        @Override
        public void actionPerformed(ActionEvent evt) {
                    
            if(registraragente == null){
                    registraragente = new Agente_ui(transito);
                    escritorio2.add(registraragente);
                    // poner la ventana y el desktop del tamaño de la ventana
                    Redimensionar(registraragente);
            }
            registraragente.setVisible(true);
        }
    }
      // pone el tamaño de la ventana y el escritorio dependiendo del jinternalframe que se abra
    public void Redimensionar (JInternalFrame ventana){
        
        // se hace un tamaño con la ventana y se le suma 50 al largo, ya que es lo que falta para que se vea
        // completa
        Dimension tamaño = new Dimension (ventana.getSize().width, ventana.getSize().height + 50 );
        this.escritorio2.setSize(tamaño);
        this.setSize(tamaño);
    }
    
    public class RegistrarMultaAL implements ActionListener{
        
        private RegistrarMulta_ui registrarmulta = null;
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (registrarmulta == null){
                registrarmulta = new RegistrarMulta_ui(transito);
                escritorio2.add(registrarmulta);
                //poner la ventana y el desktop del tamaño de la ventana
                Redimensionar(registrarmulta);
            }
            registrarmulta.setVisible(true);
        }
        
    }
}
