/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.text.SimpleDateFormat;
import java.util.Date;
import modelo.Agente;
import modelo.Categoria;
import modelo.Licencia;
import modelo.OrganismoTransito;
import modelo.Persona;
import modelo.Vehiculo;
import ui.Principal;
import ui.Vehiculo_ui;

/**
 *
 * @author salasistemas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        
        try{
        OrganismoTransito org = new OrganismoTransito();
        Vehiculo_ui vehiculo = new Vehiculo_ui(org);
        Principal transito = new Principal(org);
        transito.setVisible(true);
        
        // agregar objetos al programa
        org.AddAgente(new Agente( 1004671706L , "elsa", "pito" , (short)1234));
        org.AddVehiculo(new Vehiculo("qwerty", (short)2018, "azul" , "ferrari"));
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = sdf.parse("2018/01/01");
        
        org.AddLicencia(new Licencia(Categoria.A1, new Persona(1004671706, "elsa", "pito"), fecha));
        org.AddLicencia(new Licencia(Categoria.B2, new Persona(1004671706, "elsa", "pito"), fecha));
            
            
        
        }catch(Exception exc){
            
        }
       
    }
    
}
